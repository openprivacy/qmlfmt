# qmlfmt

`qmlfmt` in place edits the qml files specified on the command line applying fixed 4 space indent rules to them. This simple tool is aimed at projects working with QML but not using QT Creator so not benefiting from it's auto formating.
